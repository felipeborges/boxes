option ('profile', type: 'combo', choices: [ 'default', 'devel'], value: 'default')
option ('docs', type: 'feature', value: 'disabled', description: 'Build developer documentation with Valadoc')
