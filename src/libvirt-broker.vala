/* libvirt-broker.vala
 *
 * Copyright 2025 Felipe Borges
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Manages our connection with the Libvirt daemon
 *
 * //Libvirt.Broker// establishes a connection with the
 * [[https://libvirt.org/uri.html|Libvirt user-session mode daemon]] using
 * {@link GVir.Connection}.
 *
 * It manages the {@link GLib.ListStore} list store which tracks the available
 * GNOME Boxes virtual machines.
 *
 * It uses {@link GVir.Connection.fetch_domains_async} to obtain the correspondent
 * {@link GVir.Domain} objects to each virtual machine.
 *
 * @see GVir.Connection
 * @see GLib.ListStore
 *
 */
public class Libvirt.Broker: GLib.Object {
    /** A list store of {@link Libvirt.Machine} objects. */
    public GLib.ListStore model;

    private GVir.Connection connection;

    private Libvirt.VMCreator? current_vm_creation;

    public async void open_connection (GLib.Cancellable? cancellable) throws GLib.Error {
        connection = new GVir.Connection ("qemu:///session");

        yield connection.open_async (cancellable);
        yield connection.fetch_domains_async (cancellable);

        model = new GLib.ListStore (typeof (Libvirt.Machine));
        GLib.List? domains = connection.get_domains ();

        domains.foreach ((domain) => { add_machine (domain as GVir.Domain); });
        connection.domain_added.connect (domain_added);
        connection.domain_removed.connect ((domain) => { remove_machine (domain as GVir.Domain); });
    }

    private Libvirt.Machine? add_machine (GVir.Domain domain) {
        var machine = new Libvirt.Machine (domain as GVir.Domain);

        bool is_duplicate = model.find_with_equal_func (machine, (a, b) => {
            var machine_a = a as Libvirt.Machine;
            var machine_b = b as Libvirt.Machine;

            return machine_a.name == machine_b.name;
        }, null);

        if (is_duplicate) {
            debug ("Duplicate. Machine with name '%s' already exists", machine.name);

            return null;
        }

        model.append (machine);
        return machine;
    }

    private void domain_added (GVir.Domain domain) {
        Libvirt.Machine? machine = add_machine (domain);
        if (machine == null)
            return;

        post_install_setup (machine);
    }

    private void post_install_setup (Libvirt.Machine machine) {
        machine.title = current_vm_creation.title;
    }

    private void remove_machine (GVir.Domain domain) {
        foreach_machine ((machine, position) => {
            if (machine.name == domain.get_name ())
                model.remove (position);
        });
    }

    public Libvirt.Machine? get_machine_by_name (string name) {
        Libvirt.Machine? found_machine = null;
        foreach_machine ((machine) => {
            if (machine.name == name) {
                found_machine = machine;
                return;
            }
        });

        return found_machine;
    }

    public async void create_vm (Libvirt.VMCreator vm_creator) throws GLib.Error {
        this.current_vm_creation = vm_creator;

        yield vm_creator.create ();
    }

    private delegate void MachineForeachFunc (Libvirt.Machine machine, uint position);
    private void foreach_machine (MachineForeachFunc foreach_func) {
        for (int idx = 0; idx < model.get_n_items (); idx++) {
            Libvirt.Machine machine = model.get_item (idx) as Libvirt.Machine;

            foreach_func (machine, idx);
        }
    }
}
