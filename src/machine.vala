/* machine.vala
 *
 * Copyright 2023 Felipe Borges
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Represents a virtual machine. Manages lifecycle, libvirt configuration, and display connection
 *
 * //Libvirt.Machine// is the most important class in Boxes. It holds a reference
 * to the respective Libvirt domain ({@link GVir.Domain}) of this virtual machine and
 * to its QEMU DBus display connection ({@link GLib.DBusConnection}). It handles the
 * most important VM operations, such as start/stop/delete.
 *
 * A Libvirt.Machine object is created from a Libvirt virtual machine domain
 * configuration (GVir.Domain).
 *
 * The Boxes Libvirt.Machine objects collection (see {@link Boxes.Collection}) is
 * populated with existing Libvirt virtual machines by {@link Libvirt.Broker.open_connection}.
 *
 * New Libvirt.Machine objects are also created by {@link Libvirt.VMCreator},
 * which creates a brand-new {@link GVir.Domain} from a {@link Libvirt.SourceMedia}
 * and adds it to the Libvirt broker.
 *
 * See also [[https://libvirt.org/formatdomain.html]]
 *
 * @see GVir.Domain
 * @see GLib.DBusConnection
 * @see Boxes.Collection
 * @see Libvirt.Broker
 * @see Libvirt.VMCreator
 * @see Libvirt.SourceMedia
 */
public class Libvirt.Machine: GLib.Object {
    public GVir.Domain domain;
    private GVir.DomainState domain_state {
        get {
            var state = GVir.DomainState.NONE;
            try {
                state = this.domain.get_info ().state;
            } catch (GLib.Error error) {
                warning ("Failed to get domain information: '%s'", this.title);
            }

            return state;
        }
    }
    public string state {
        get {
            if (domain_state == GVir.DomainState.SHUTOFF)
                return "Powered Off";

            return "Running";
        }
    }

    /** The [[https://www.qemu.org/docs/master/interop/dbus-display.html|QEMU DBus Display]] display connection for this VM. */
    public GLib.DBusConnection connection { private set; get; }

    public GVirConfig.Domain config;
    private string _title;
    public string title {
        get {
            _title = VMConfigurator.get_machine_title (this);
            return _title;
        }

        set {
            VMConfigurator.update_machine_title (this, value);
            notify_property ("title");
        }
    }

    public string name {
        get {
            return domain.get_name ();
        }
    }

    private GLib.Variant _variant_name;
    public GLib.Variant variant_name {
        get {
            _variant_name = new GLib.Variant ("s", name);

            return _variant_name;
        }
    }

    public signal void stopped ();

    public Machine (GVir.Domain domain) {
        this.domain = domain;
        this.domain.stopped.connect (() => { this.stopped (); });

        try {
            this.config = this.domain.get_config (GVir.DomainXMLFlags.INACTIVE);
        } catch (GLib.Error error) {
            warning ("Failed to get domain config: %s", error.message);
        }
    }

    public delegate void OpenDisplayCallback (bool result, string? reason = null);
    public async void open_display (owned OpenDisplayCallback callback) throws GLib.Error {
        if (VMConfigurator.get_supported_display (this) == null) {
            if (!VMConfigurator.migrate_from_spice (this)) {
                callback (false, "Failed to migrate machine from SPICE");
                return;
            }
        }
        // Start the VM if it is shut off
        if (this.domain_state == GVir.DomainState.SHUTOFF) {
            yield this.start ();
        }

        string? sock_address = VMConfigurator.get_sock_address (this);
        this.acquire_dbus_connection (sock_address, (owned)callback);
    }

    private void acquire_dbus_connection (string address, owned OpenDisplayCallback callback) {
        if (this.connection != null && !this.connection.closed) {
            callback (true);

            return;
        }

        GLib.DBusConnectionFlags flags = GLib.DBusConnectionFlags.AUTHENTICATION_CLIENT |
                                         GLib.DBusConnectionFlags.MESSAGE_BUS_CONNECTION;
        string? reason = null;
        try {
            this.connection = new GLib.DBusConnection.for_address_sync (address, flags);
        } catch (GLib.Error error) {
            reason = "Failed to start DBus connection: " + error.message;
        }

        callback (this.connection != null, reason);
    }

    private async void start () throws GLib.Error {
        var flags = GVir.DomainStartFlags.FORCE_BOOT | GVir.DomainStartFlags.AUTODESTROY;
            yield this.domain.start_async (flags, null);
    }

    public signal void deleted ();
    public async void delete () throws GLib.Error {
        domain.delete (GVir.DomainDeleteFlags.SAVED_STATE |
                       GVir.DomainDeleteFlags.SNAPSHOTS_METADATA |
                       GVir.DomainDeleteFlags.REMOVE_NVRAM);
        deleted ();
    }

    public void force_shutdown () {
        debug ("Force shutdown '%s'", this.title);

        try {
            this.domain.stop (0);
        } catch (GLib.Error error) {
            warning ("Failed to shutdown '%s': %s", this.title, error.message );
        }
    }
}
