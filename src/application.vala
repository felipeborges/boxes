/* application.vala
 *
 * Copyright 2023 Red Hat, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Config;
using GVir;

public class Boxes.Application : Adw.Application {
    public static Boxes.Application controller;

    private Libvirt.Broker libvirt_broker;

    public GLib.Cancellable cancellable = new GLib.Cancellable ();

    public Application () {
        Object (application_id: Config.APPLICATION_ID, flags: ApplicationFlags.DEFAULT_FLAGS);

        controller = this;
    }

    construct {
        ActionEntry[] action_entries = {
            { "about", this.on_about_action },
            { "preferences", this.on_preferences_action },
            { "quit", this.quit }
        };
        this.add_action_entries (action_entries, this);
        this.set_accels_for_action ("app.quit", {"<primary>q"});
    }

    public async void create_vm (Libvirt.VMCreator vm_creator) {
        try {
            yield libvirt_broker.create_vm (vm_creator);
        } catch (GLib.Error error) {
            show_error ("Failed to create VM", error.message);
        }
    }

    public Libvirt.Machine? get_machine_by_name (string name) {
        return libvirt_broker.get_machine_by_name (name);
    }

    public override void activate () {
        base.activate ();
        var win = this.active_window;
        if (win == null) {
            win = new Boxes.Window (this);
        }
        win.present ();

        activate_async.begin ();
    }

    private async void activate_async () {
        libvirt_broker = new Libvirt.Broker ();
        var window = this.active_window as Boxes.Window;

        try {
            yield libvirt_broker.open_connection (cancellable);
        } catch (GLib.Error error) {
            show_error ("Failed to initialize virtualization service", error.message, true);
            return;
        }

        window.show_collection_view (libvirt_broker.model);
    }

    private void show_error (string message, string description, bool fatal = false) {
        var window = this.active_window as Boxes.Window;

        window.show_error (message, description, fatal);
    }

    public override void shutdown () {
        cancellable.cancel ();

        base.shutdown ();
    }

    private void on_about_action () {
        var about = new Adw.AboutDialog () {
            application_name = "Boxes",
            application_icon = Config.APPLICATION_ID,
            developer_name = "The GNOME Project",
            version = Config.VERSION,
            developers = { "Felipe Borges" },
            artists = { "Jakub Steiner" },
            copyright = "© 2024 Red Hat, Inc",
        };

        about.present (this.active_window);
    }

    private void on_preferences_action () {
        message ("app.preferences action activated");
    }
}
