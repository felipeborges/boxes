/* source-media.vala
 *
 * Copyright 2025 Felipe Borges
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Represents a {@link GLib.File} file in the disk that can be used to create a virtual machine
 *
 * A //Libvirt.SourceFile// object represents an virtual machine creation source,
 * such as a bootable ISO file or a virtual machine disk file (qcow2, raw, etc...)
 *
 * It is responsible for parsing the source file metadata to detect its content-type
 * and operating system.
 *
 * See also [[https://libvirt.org/storage.html]]
 */
public class Libvirt.SourceMedia: GLib.Object {
    private GLib.File file;

    private const string[] installer_media_content_types = {
        "application/x-cd-image",
    };
    private const string[] disk_media_content_types = {
        "application/x-qemu-disk",
        "application/x-raw-disk-image",
    };

    public bool is_supported {
        get {
            return is_installer_media || is_disk_media;
        }
    }

    /** Whether this is an operating system installation source (for example, .iso) */
    public bool is_installer_media {
        get {
            return media_matches_content_type (true);
        }
    }

    /** Whether this is an existing virtual machine disk with an installed operating system (for example, .qcow2) */
    public bool is_disk_media {
        get {
            return media_matches_content_type (false);
        }
    }

    public SourceMedia (GLib.File file) {
        this.file = file;
    }

    private const string ATTRIBUTE_HOST_PATH = "xattr::document-portal.host-path";
    public async string get_path () throws GLib.Error {
        GLib.FileInfo file_info = yield file.query_info_async (ATTRIBUTE_HOST_PATH,
                                                               GLib.FileQueryInfoFlags.NONE,
                                                               0,
                                                               null); // FIXME: cancellable
        string? host_path = file_info.get_attribute_string (ATTRIBUTE_HOST_PATH);
        if (host_path != null)
            return host_path;

        return file.get_path ();
    }

    private bool media_matches_content_type (bool installed) {
        var content_types = installed ? installer_media_content_types : disk_media_content_types;
        foreach (var content_type in content_types) {
            try {
                FileInfo info = file.query_info ("standard::content-type,standard::fast-content-type", 0);

                if (ContentType.is_a (info.get_content_type (), content_type) ||
                    ContentType.is_a (info.get_attribute_string ("standard::fast-content-type"), content_type)) {

                    return true;
                }
            } catch (GLib.Error error) {
                warning ("Unable to query content type for '%s': %s", file.get_path (), error.message);
            }
        }

        return false;
    }

    public Osinfo.Os? get_os () {
        Osinfo.Os? os = null;
        try {
            Osinfo.Media media = Osinfo.Media.create_from_location (file.get_path (), null);

            var db_loader = new Osinfo.Loader ();
            db_loader.process_default_path ();
            var db = db_loader.get_db ();

            if (db.identify_media (media))
                os = media.get_os ();
            else
                os = db.get_os ("http://libosinfo.org/linux/2022");
        } catch (GLib.Error error) {
            warning ("Operating system not detected: %s", error.message);
        }

        return os;
    }
}
