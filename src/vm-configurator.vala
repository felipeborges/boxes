/* vm-configurator.vala
 *
 * Copyright 2025 Felipe Borges
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Configures the {@link GVirConfig.Domain} of a {@link Libvirt.Machine}
 *
 * @see Libvirt.Machine
 * @see GVirConfig.Domain
 *
 */
public class Libvirt.VMConfigurator: GLib.Object {

    public static string get_machine_title (Libvirt.Machine machine) {
        try {
            GVirConfig.Domain config = machine.domain.get_config (GVir.DomainXMLFlags.INACTIVE);
            return config.get_title ();
        } catch (GLib.Error error) {
            warning ("Failed to fetch machine title, fallback to domain name: %s", error.message);
        }

        return machine.name;
    }

    public static void update_machine_title (Libvirt.Machine machine, string title) {
        try {
            GVirConfig.Domain config = machine.domain.get_config (GVir.DomainXMLFlags.INACTIVE);
            config.set_title (title);
            machine.domain.set_config (config);
        } catch (GLib.Error error) {
            critical ("Failed to update machine title: %s", error.message);
        }
    }

    public static string? get_sock_address (Libvirt.Machine machine) {
        var device = get_supported_display (machine);
        if (device == null)
            return null;

        return device.get_address ();
    }

    public static GVirConfig.DomainGraphicsDBus? get_supported_display (Libvirt.Machine machine) {
        GLib.List<GVirConfig.DomainDevice>? devices = null;
        try {
            GVirConfig.Domain runtime_config = machine.domain.get_config (GVir.DomainXMLFlags.NONE);
            devices = runtime_config.get_devices ();
        } catch (GLib.Error error) {
            critical ("Failed to load display config: %s", error.message);
        }

        foreach (var device in devices) {
            if (device is GVirConfig.DomainGraphicsDBus) {
                return device as GVirConfig.DomainGraphicsDBus;
            }
        }

        return null;
    }

    public static bool migrate_from_spice (Libvirt.Machine machine) {
        GLib.List<GVirConfig.DomainDevice> new_devices = null;
        var devices = machine.config.get_devices ();
        foreach (var device in devices) {
            if (device is GVirConfig.DomainGraphicsSpice ||
                device is GVirConfig.DomainChannel ||
                device is GVirConfig.DomainChardevSourceSpiceVmc ||
                device is GVirConfig.DomainRedirdev ||
                device is GVirConfig.DomainControllerUsb ||
                device is GVirConfig.DomainSound) {
                continue;
            }

            if (device is GVirConfig.DomainVideo) {
                var video = device as GVirConfig.DomainVideo;
                video.set_model (GVirConfig.DomainVideoModel.VIRTIO);
            }

            var device_xml = device.to_xml ();
            if (device_xml.has_prefix ("<audio"))
                continue;

            new_devices.prepend (device);
        }

        var dbus_display = new GVirConfig.DomainGraphicsDBus ();
        new_devices.prepend (dbus_display);

        new_devices.reverse ();
        machine.config.set_devices (new_devices);

        try {
            machine.domain.set_config (machine.config);
        } catch (GLib.Error error) {
            critical ("Failed to move away from SPICE: %s", error.message);

            return false;
        }

        return true;
    }
}
