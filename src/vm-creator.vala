/* vm-creator.vala
 *
 * Copyright 2025 Felipe Borges
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Creates a {@link Libvirt.Machine} from a {@link Libvirt.SourceMedia} file
 *
 * //Libvirt.VMCreator// is a [[https://en.wikipedia.org/wiki/Builder_pattern|builder object]]
 * for constructing virtual machines from a source file, handling user preferences.
 *
 * //Libvirt.VMCreator// is initialized with a {@link Libvirt.SourceMedia} file,
 * and its object properties reflect the recommended/desired configuration for the
 * virtual machine being created.
 *
 * Once the source media file has been detected and user preferences have been gathered,
 * {@link Libvirt.VMCreator.create} uses [[https://linux.die.net/man/1/virt-install|virt-install]]
 * to create a Libvirt virtual machine.
 *
 * @see Libvirt.Machine
 * @see Libvirt.SourceMedia
 * @see Libvirt.Broker
 *
 */
public class Libvirt.VMCreator: GLib.Object {
    private Libvirt.SourceMedia? source_media { get; set; }

    private Osinfo.Os? _os;
    public Osinfo.Os? os {
        get {
            _os = source_media.get_os ();
            return _os;
        }
    }

    /** The amount of ''memory'' available to the VM being created. */
    public uint ram_memory { get; set; }

    public string title { get; set; }

    public VMCreator (Libvirt.SourceMedia source_media) {
        this.source_media = source_media;
        title = os.get_name ();
    }

    private async void load_recommended_resources () throws GLib.Error {
        ram_memory = 6144;
    }

    public async void create () throws GLib.Error {
        yield load_recommended_resources ();

        string[] args = build_args ();

        string path = yield source_media.get_path ();
        if (source_media.is_installer_media) {
            debug ("Source media is an installer media. Booting from installation cdrom...");

            args += ("--disk=%s,device=cdrom").printf (path);
        } else {
            debug ("Source media is a VM. Starting import...");

            args += "--import";
            args += ("--disk=%s,bus=sata").printf (path);
        }

        yield run_virt_install (args);
    }

    private async void run_virt_install (string[] args) throws GLib.Error {
        if (!Process.spawn_async (null, args, null, SpawnFlags.SEARCH_PATH, null, null)) {
            //throw new GLib.Error
            warning ("Failed to launch virt-install with: %s", string.join (" ", args));
        }
    }

    private string[] build_args () {
        string[] args = {};

        args += "virt-install";
        args += "--connect=qemu:///session";
        args += "--memory=" + ram_memory.to_string ();
        args += "--vcpus=2";
        args += "--graphics=dbus";
        args += "--boot=cdrom,hd,menu=on";

        /* Let virt-install do the Osinfo Os detection and fallback to linux2022. */
        args += "--osinfo";
        args += "detect=on,name=" + os.get_short_id ();

        return args;
    }
}
