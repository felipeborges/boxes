/* assistant.vala
 *
 * Copyright 2025 Red Hat, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author(s):
 *  Felipe Borges <felipeborges@gnome.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * The page where a machine is configured before creation
 *
 */
[GtkTemplate (ui = "/org/gnome/Boxes/assistant.ui")]
public class Boxes.Assistant : Adw.NavigationPage {
    private Libvirt.VMCreator vm_creator;

    [GtkChild]
    private unowned Adw.EntryRow name_row;

    public Assistant (GLib.File file) {
        var source_media = new Libvirt.SourceMedia (file);
        if (!source_media.is_supported) {
            critical ("File type is not supported: %s", file.get_basename ());
            return;
        }

        vm_creator = new Libvirt.VMCreator (source_media);;
        name_row.bind_property ("text", vm_creator, "title", GLib.BindingFlags.DEFAULT);
        name_row.text = vm_creator.title;
    }

    public signal void machine_created ();
    [GtkCallback]
    private async void create () {
        yield Application.controller.create_vm (vm_creator);
        machine_created ();
    }
}
