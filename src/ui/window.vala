/* window.vala
 *
 * Copyright 2023 Red Hat, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

[GtkTemplate (ui = "/org/gnome/Boxes/window.ui")]
public class Boxes.Window : Adw.ApplicationWindow {
    [GtkChild]
    private unowned Adw.NavigationView navigation;
    public Boxes.Collection collection;

    public Window (Gtk.Application app) {
        Object (application: app);

        if (app.application_id == "org.gnome.BoxesDevel") {
            add_css_class ("devel");
        }

        /* We don't want to animate the first page transition */
        navigation.set_animate_transitions (false);

        var css_provider = new Gtk.CssProvider ();
        css_provider.load_from_resource ("/org/gnome/Boxes/style.css");
        Gtk.StyleContext.add_provider_for_display (get_display (),
                                                   css_provider,
                                                   Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        install_action ("window.open-machine-preferences", "s",
                        (Gtk.WidgetActionActivateFunc) open_machine_preferences);
    }

    public void show_collection_view (GLib.ListStore model) {
        if (collection == null) {
            collection = new Boxes.Collection ();

            collection.notify["selected-item"].connect (() => {
                var machine = this.collection.selected_item as Libvirt.Machine;
                connect_to_display.begin (machine);
            });

            this.navigation.push (this.collection);
        }

        this.collection.set_model (model);
        this.navigation.pop_to_page (this.collection);

        /* After the first page has been pushed, we can animate transitions. */
        navigation.set_animate_transitions (true);
    }

    public void show_error (string title, string description, bool fatal = false) {
        navigation.push (new Boxes.ErrorPage () {
            title = title,
            description = description,
            fatal = fatal,
        });
    }

    public async void connect_to_display (Libvirt.Machine machine, bool is_main_window = true) {
        machine.stopped.connect (() => { pop_display (is_main_window); });

        try {
            yield machine.open_display ((opened, reason) => {
                if (!opened) {
                    this.navigation.push (new Boxes.ErrorPage () {
                        title = "Failed to open display",
                        description = reason,
                    });

                    return;
                }

                var display = new Boxes.Display (machine) {
                    can_pop = is_main_window,
                };

                display.close.connect (() => { pop_display (is_main_window); });

                this.navigation.push (display);
            });
        } catch (GLib.Error error) {
            show_error ("Failed to start virtual machine", error.message);
        }
    }

    private void pop_display (bool is_main_window) {
        if (is_main_window)
            navigation.pop_to_page (collection);
        else
            close ();
    }

    public void show_assistant (GLib.File file) {
        var assistant_page = new Boxes.Assistant (file);
        navigation.push (assistant_page);

        assistant_page.machine_created.connect (() => { navigation.pop (); });
    }

    public void open_machine_preferences (Gtk.Widget widget, GLib.Variant parameter) {
        var domain_name = parameter.get_string ();
        Libvirt.Machine machine = Application.controller.get_machine_by_name (domain_name);
        Adw.NavigationPage preferences_page = new Boxes.Preferences (machine);
        navigation.push (preferences_page);

        machine.deleted.connect (() => navigation.pop_to_page (collection));
    }
}
