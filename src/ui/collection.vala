/* collection.vala
 *
 * Copyright 2023 Red Hat, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author(s):
 *  Felipe Borges <felipeborges@gnome.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Is the grid page where the list of available virtual machines is displayed
 *
 */
[GtkTemplate (ui = "/org/gnome/Boxes/collection.ui")]
public class Boxes.Collection : Adw.NavigationPage {
    [GtkChild]
    private unowned Gtk.NoSelection selection;
    [GtkChild]
    private unowned Gtk.Stack stack;

    public Libvirt.Machine selected_item { get; set; }

    [GtkCallback]
    private async void on_add_button_clicked () {
        var chooser = new Gtk.FileDialog () {
            title = "Create Virtual Machine", // FIXME: make translatable
            modal = true,
        };

        GLib.File? file = null;
        try {
            file = yield chooser.open (Application.controller.active_window,
                                       Application.controller.cancellable);
        } catch (GLib.FileError error) {
            critical ("Failed to create virtual machine: %s", error.message);
            return;
        } catch (GLib.Error error) {
            /* User dismissed the file dialog */
            return;
        }

        var window = Application.controller.active_window as Boxes.Window;
        window.show_assistant (file);
    }

    [GtkCallback]
    private void on_item_activated (uint position) {
        var new_selection = get_machine (position);
        if (this.selected_item == new_selection)
            this.notify_property ("selected-item");
        else
            this.selected_item = new_selection;
    }

    public void set_model (GLib.ListModel? model) {
        this.selection.set_model (model);
        selection.items_changed.connect (on_items_changed);

        set_visible_page ();
    }

    private void on_items_changed (uint position, uint removed, uint added) {
        if (added > 0 && removed == 0) {
            selected_item  = get_machine (position);
        }

        set_visible_page ();
    }

    private void set_visible_page () {
        bool is_empty = selection.model.get_n_items () == 0;
        stack.set_visible_child_name (is_empty ? "empty-state" : "collection-view");
    }

    private Libvirt.Machine get_machine (uint position) {
        var model = selection.get_model ();
        return model.get_item (position) as Libvirt.Machine;
    }
}
