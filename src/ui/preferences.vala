/* preferences.vala
 *
 * Copyright 2023 Red Hat, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author(s):
 *  Felipe Borges <felipeborges@gnome.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Is the grid page where the list of available virtual machines is displayed
 *
 */
[GtkTemplate (ui = "/org/gnome/Boxes/preferences.ui")]
public class Boxes.Preferences : Adw.NavigationPage {
    private Libvirt.Machine machine;

    [GtkChild]
    private unowned Adw.EntryRow name_row;

    [GtkChild]
    private unowned Adw.AlertDialog delete_machine_dialog;

    public Preferences (Libvirt.Machine machine) {
        this.machine = machine;

        machine.bind_property ("title", name_row, "text", GLib.BindingFlags.BIDIRECTIONAL | GLib.BindingFlags.SYNC_CREATE);

        title = machine.title;
    }

    [GtkCallback]
    private async void on_delete_virtual_machine_response_cb () {
        try {
            yield machine.delete ();
        } catch (GLib.Error error) {
            warning (error.message);
            // TODO: show_error page
        }
    }

    [GtkCallback]
    private async void open_in_new_window () {
        Boxes.Window new_window = new Boxes.Window (Application.controller);

        new_window.present ();
        yield new_window.connect_to_display (machine, false);
    }
}
