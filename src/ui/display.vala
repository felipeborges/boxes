/* display.vala
 *
 * Copyright 2023 Red Hat, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author(s):
 *  Felipe Borges <felipeborges@gnome.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
using Mks;

/**
 * Manages the {@link Mks.Display} widget and the virtual machine display page interface
 */

[GtkTemplate (ui = "/org/gnome/Boxes/display.ui")]
public class Boxes.Display : Adw.NavigationPage {
    [GtkChild]
    private unowned Mks.Display mks_display;

    private Mks.Screen screen;
    private Mks.Session session;

    private Libvirt.Machine machine;

    public Display (Libvirt.Machine machine) {
        install_action ("display.force-shutdown", null,
                        (Gtk.WidgetActionActivateFunc)force_shutdown);
        install_action ("display.open-machine-preferences", null,
                        (Gtk.WidgetActionActivateFunc)open_machine_preferences);

        this.machine = machine;
        machine.bind_property ("title", this, "title", GLib.BindingFlags.DEFAULT);
        title = machine.title ?? title;

        try {
            this.session = new Mks.Session.for_connection_sync (machine.connection, null);
            this.screen = this.session.ref_screen ();

            this.mks_display.set_screen (this.screen);

            var picture = this.mks_display.get_first_child ();
            picture.set_cursor (new Gdk.Cursor.from_name ("default", null));
        } catch (GLib.Error error) {
            critical ("Failed to setup display: %s", error.message);
        }
    }

    private void force_shutdown (Gtk.Widget widget, GLib.Variant? parameter) {
        this.machine.force_shutdown ();
    }

    private void open_machine_preferences (Gtk.Widget widget, GLib.Variant? parameter) {
        var window = Application.controller.active_window as Boxes.Window;
        window.open_machine_preferences (this as Gtk.Widget, machine.variant_name);
    }

    public override bool grab_focus () {
        return this.mks_display.grab_focus ();
    }

    public signal void close ();
    [GtkCallback]
    public void goto_grid_clicked_cb () {
        close ();
    }
}
